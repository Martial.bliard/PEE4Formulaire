CREATE TABLE `MEDIA` (
	`id` INT NOT NULL,
	`nom` varchar(100) NOT NULL,
	`titre` varchar(100) NOT NULL,
	`dateCreation` DATE NOT NULL,
	`dateMiseEnLigne` DATE NOT NULL,
	`dateExpiration` DATE NOT NULL,
	`extension` varchar(10) NOT NULL,
	`emplacement` TEXT NOT NULL,
	`nomRubrique` varchar(50) NOT NULL,
	`nomService` varchar(50) NOT NULL,
	`nomCategorie` varchar(50) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `RUBRIQUE` (
	`nom` INT NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`nom`)
);

CREATE TABLE `CATEGORIE` (
	`nom` INT NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`nom`)
);

CREATE TABLE `SERVICE` (
	`nom` INT NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`nom`)
);

CREATE TABLE `S/R` (
	`nomR` varchar(50) NOT NULL,
	`nomS` varchar(50) NOT NULL
);

CREATE TABLE `Untitled` (
	`nomC` varchar(50) NOT NULL,
	`nomS` varchar(50) NOT NULL
);

ALTER TABLE `MEDIA` ADD CONSTRAINT `MEDIA_fk0` FOREIGN KEY (`nomRubrique`) REFERENCES `RUBRIQUE`(`nom`);

ALTER TABLE `MEDIA` ADD CONSTRAINT `MEDIA_fk1` FOREIGN KEY (`nomService`) REFERENCES `SERVICE`(`nom`);

ALTER TABLE `MEDIA` ADD CONSTRAINT `MEDIA_fk2` FOREIGN KEY (`nomCategorie`) REFERENCES `CATEGORIE`(`nom`);

ALTER TABLE `S/R` ADD CONSTRAINT `S/R_fk0` FOREIGN KEY (`nomR`) REFERENCES `RUBRIQUE`(`nom`);

ALTER TABLE `S/R` ADD CONSTRAINT `S/R_fk1` FOREIGN KEY (`nomS`) REFERENCES `SERVICE`(`nom`);

ALTER TABLE `Untitled` ADD CONSTRAINT `Untitled_fk0` FOREIGN KEY (`nomC`) REFERENCES `CATEGORIE`(`nom`);

ALTER TABLE `Untitled` ADD CONSTRAINT `Untitled_fk1` FOREIGN KEY (`nomS`) REFERENCES `SERVICE`(`nom`);
